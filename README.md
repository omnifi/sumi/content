# Omnifi Content

This project provides the content for the [Omnifi](https://omnifi.foundation) web site and is included as a git submodule in the web project.